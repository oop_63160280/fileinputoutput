/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.fileinputoutput;

import java.io.Serializable;

/**
 *
 * @author User
 */
public class Rectangle implements Serializable{
    private int width;
    private int hingt;

    public Rectangle(int width, int hingt) {
        this.width = width;
        this.hingt = hingt;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHingt() {
        return hingt;
    }

    public void setHingt(int hingt) {
        this.hingt = hingt;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "width=" + width + ", hingt=" + hingt + '}';
    }
    
}
